# containers

This repo is for my self-build containers.

# todo

- [x] snipe-it
- [x] ircd
- [x] dovecot
- [x] phpsrv
- [ ] rsyncd
- [ ] admini
- [ ] torrent
- [ ] indexer
- [ ] idm
- [ ] photoshow
- [ ] photosync
- [x] webpub

# vanilla image

- [x] mariadb-10
- [ ] jellyfin
- [ ] shinobi?
- [ ] uptimekuma
- [ ] photoshare
- [ ] photoprism

# web app

- [ ] webscanner2
