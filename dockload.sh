#!/bin/sh
#
# Load all the images in the given directory
#

if [ $# -eq 0 ] ; then
  echo "Usage:"
  echo "  $0  directory"
  exit 1
fi
set -euf
( set -o pipefail 2>/dev/null ) && set -o pipefail

find "$1" ! -name '*.tags' -type f | while read f
do
  [ ! -f "$f" ] && continue
  name="$(basename "$f")"
  echo "Load $name... "
  docker load --input="$f"
done
