IMAGE ?= $(shell basename "$$(pwd)")

clean:
	[ -f start ] && docker stop $$(cat start) && sleep 3 || :
	[ -f build ] && docker rmi $(IMAGE) || :
	rm -fv files.tar.gz build start
