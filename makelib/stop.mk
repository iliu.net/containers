stop:
	[ -f start ] && ( docker stop $$(cat start) || :) && rm -fv start \
		|| echo Not running
