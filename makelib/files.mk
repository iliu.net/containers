ADDON_FILES ?= $(shell find files -type f)

files.tar.gz: $(ADDON_FILES)
	tar -C files -zcvf $@ .
