IMAGE ?= $(shell basename "$$(pwd)")

build: Dockerfile files.tar.gz
	[ -f start ] && docker stop $$(cat start) && sleep 3 || :
	docker rmi $(IMAGE) || :
	docker buildx build -t $(IMAGE) $(BUILD_X_ARGS) .
	docker image inspect $(IMAGE) >$@
