DOCKER_SHELL ?= sh

shell: start
	docker exec -it $$(cat start) $(DOCKER_SHELL) -il
