#~ START_OPTS = --detach
#~ START_OPTS = -it

# Normally we auto remove containers, define DOCKER_AUTO_REMOVE to
# an empty string to remove this behaviour.  This is useful to
# debug container startup failures.

DOCKER_AUTO_REMOVE ?= --rm
START_OPTS ?= -it
IMAGE ?= $(shell basename "$$(pwd)")

start: build
	name="$(IMAGE).$$$$" ; \
		echo "$$name" | tee $@ ; \
		docker run \
		$(PORTS) $(VOLUMES) $(ENV) \
		$(START_OPTS) $(DOCKER_AUTO_REMOVE) --name $$name $(IMAGE) || ( rm -f $@ ; exit 1) ; \
		( docker inspect -f '{{.State.Status}}' $$name| grep -q running ) || \
			rm -fv $@

