#!/bin/sh
#
# Load all the images in the given directory
#

if [ $# -eq 0 ] ; then
  echo "Usage:"
  echo "  $0  directory"
  exit 1
fi
set -euf
( set -o pipefail 2>/dev/null ) && set -o pipefail

: ${TAG_NAMESPACE:=testing}
if [ -n "${CI_COMMIT_SHORT_SHA:-}" ] ; then
  if [ "$CI_COMMIT_REF_NAME" = "$CI_DEFAULT_BRANCH" ] ; then
    TAG_REF="$CI_COMMIT_SHORT_SHA"
  else
    TAG_REF="$CI_COMMIT_SHORT_SHA.$CI_COMMIT_REF_NAME"
  fi
else
  TAG_REF=$(git rev-parse --short HEAD)
fi

TNOW=$(date +%y.%m)

find "$1" ! -name '*.tags' -type f | while read f
do
  [ ! -f "$f" ] && continue
  if [ -f "$f.tags" ] ; then
    xtags=$(cat $f.tags)
  else
    xtags=""
  fi

  name="$(basename "$f")"
  image_name="$TAG_NAMESPACE/$name"
  for tag in "latest" "$TNOW-$TAG_REF" "$TNOW" $xtags
  do
    echo publish "$image_name:$tag"
    docker tag "$name" "$image_name:$tag"
    docker push "$image_name:$tag"
  done
done
