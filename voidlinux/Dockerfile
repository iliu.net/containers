FROM voidlinux/voidlinux-musl:latest

RUN <<EOT sh
  set -ex
  # Update repo URL
  echo repository=https://repo-default.voidlinux.org/current/musl > /etc/xbps.d/10-repository-main.conf
  echo y | env XBPS_ARCH=x86_64-musl xbps-install -y -A -S -u xbps
  rm -f /etc/xbps.d/10-repository-main.conf || :
EOT

RUN <<EOT sh
  set -ex
  # For some reason we can't upgrade base-files normally in a docker build
  echo y | xbps-remove -F base-files
  echo y | env XBPS_ARCH=x86_64-musl xbps-install -y -A -S base-files
  echo y | env XBPS_ARCH=x86_64-musl xbps-install -y -A -S -u
  echo y | env XBPS_ARCH=x86_64-musl xbps-install -y -A bash
  echo y | env XBPS_ARCH=x86_64-musl xbps-install -y -A \
    base-voidstrap acl-progs dialog netcat p7zip patch pwgen pv \
    rsync wget unzip zip iputils dcron void-repo-nonfree \
    screen tmux
  svdir=/etc/runit/runsvdir/default

  for sv in crond uuidd statd rpcbind dbus udevd elogind sshd
  do
    [ -e /etc/sv/\$sv ] || continue
    rm -f \$svdir/\$sv
    ln -s /etc/sv/\$sv \$svdir/\$sv
  done

  rm -fv /etc/runit/core-services/*-filesystems.sh
  rm -fv /etc/runit/runsvdir/default/agetty-*
  find /var/cache/xbps -type f -print0 | xargs -0 -r rm -v

EOT

CMD [ "/sbin/runit" ]
