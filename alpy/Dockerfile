ARG alpine_ver=3.18.3
FROM alpine:$alpine_ver
ENV TZ UTC
LABEL description="Basic Alpine Linux image with supervisord, logrotate and rsync"

RUN <<EOT sh
  set -ex
  apk update
  echo welcome > /etc/motd

  # Basic infra
  apk add tzdata ca-certificates supervisor rsync logrotate
  mkdir -p /etc/supervisor.d
  # Hack the Supervisord UI
  ( set +x
    echo "Tweaking Supervisord UI"
    pylib=\$(find /usr/lib -mindepth 1 -maxdepth 1 -type d -name python3*)
    if [ -n "\$pylib" ] && [ -d "\$pylib" ] ; then
      html="\$pylib/site-packages/supervisor/ui/status.html"
      if [ -f "\$html" ] ; then
	[ ! -f "\$html-bak" ] && cp -av "\$html" "\$html-bak"
	sed -e s'/^/:/' "\$html-bak" | (
	  while read -r line
	  do
	    echo "\$line"
	    if ( echo "\$line" | grep -q -i '</title>') ; then
	      echo '<meta name="viewport" content="width=device-width, initial-scale=0.8" />'
	    elif ( echo "\$line" | grep -q -i '<body>') ; then
		      echo '<a href="/">Home</a>'
	    fi
	  done
	) | sed -e 's/^://' > "\$html"
      fi
    fi
  )

EOT


ADD files.tar.gz /
HEALTHCHECK CMD /usr/local/sbin/healthcheck.sh

EXPOSE 9001/tcp
VOLUME /apkcache

CMD [ "/startup.sh" ]
