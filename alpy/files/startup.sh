#!/bin/sh
if [ $# -gt 0 ] ; then
  if type "$1" >/dev/null 2>&1 ; then
    exec "$@"
    exit $?
  fi
  exec /usr/bin/supervisord "$@"
  exit $?
fi

# Set-up TZ
if [ -n "${TZ:-}" ] ; then
  cp -a /usr/share/zoneinfo/$TZ /etc/localtime
  echo "$TZ" > /etc/timezone
fi

# Make sure we have network connectivity
gw=$(route -n | awk '$1 == "0.0.0.0" { print $2 }')
wait_time=600
cnt=0
net_up=false
while [ $cnt -lt $wait_time ]
do
  cnt=$(expr $cnt + 1)
  if ping -c 1 $gw ; then
    net_up=true
    break
  fi
  sleep 1
done
if ! $net_up ; then
  echo "Time-out waiting for network"
  exit 1
fi

if [  ! -f /apkcache/README.md ] ; then
  cat > /apkcache/README.md <<-'_EOF_'
	# APKCACHE

	This directory is used to host a persinstant/shared cache for
	apk files.

	To enable the apk cache, mount a volume in /apkcache and create a file
	named `.is_mounted` in that directory.

	If no `.is_mounted` file is found, the apkcache directory will be IGNORED!
	_EOF_
fi

# Create and use a shared apkcache
if [ -f /apkcache/.is_mounted ] ; then
  version=$(cut -d. -f-2 /etc/alpine-release)
  mkdir -p /apkcache/$version/cache
  if [ ! -L /etc/apk/cache ] ; then
    ln -s /apkcache/$version/cache /etc/apk/cache
  fi
  apk update
fi

interactive_shell="supervisorctl"
backup_shell="/bin/sh -il"

if [ -f /addons.sh ] ; then
  . /addons.sh
fi

exec 3<&0
exec </dev/null
(
  sleep 3
  exec <&-
  exec 0<&3
  while tty
  do
    echo "exit to run $backup_shell"
    $interactive_shell
    echo "exit to run $interactive_shell"
    $backup_shell
  done
) &
exec /usr/bin/supervisord -n -c /etc/supervisord.conf </dev/null
