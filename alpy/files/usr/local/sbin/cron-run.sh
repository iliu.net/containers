#!/bin/sh
#
# Run a job and report the results to uptime-kuma
#
set -o pipefail
set -euf

###$_begin-include: stderr.sh

stderr() {
  echo "$@" 1>&2
}

###$_end-include: stderr.sh
###$_begin-include: urlencode.sh


urlencode() {
  local l=${#1} i=0
  while [ $i -lt $l ]
  do
    local c=${1:$i:1}
    case "$c" in
      [a-zA-Z0-9.~_-]) printf "$c" ;;
      ' ') printf + ;;
      *) printf '%%%.2X' "'$c"
    esac
    i=$(expr $i + 1)
  done
}

urldecode() {
  local data=${1//+/ }
  printf '%b' "${data//%/\\x}"
}

###$_end-include: urlencode.sh
###$_begin-include: rotate.sh

rotate() {
  local count=10 gz=

  while [ $# -gt 0 ]
  do
    case "$1" in
      --count=*) count=${1#--count=} ;;
      --compress) gz=.gz ;;
      --no-compress) gz= ;;
      *) break ;;
    esac
    shift
  done

  [ $# -ne 1 ] && return

  local f="$(readlink -f "$1")" t i j
  if [ -d "$f" ] ; then
    gz="" # Compression is not allowed for directories
  elif [ ! -s "$f" ] ; then
    return 0 # Skip missing or empty files
  fi

  j=$count
  while [ $j -gt 0 ]
  do
    i=$(expr $j - 1) || :
    if [ $j -eq $count ] ; then
      if [ -f "$f.$i$gz" ] ; then
	rm -f "$f.$i$gz"
      elif [ -d "$f.$i" ] ; then
	rm -rf "$f.$i"
      fi
    else
      [ -e "$f.$i$gz" ] && mv "$f.$i$gz" "$f.$j$gz"
    fi
    j=$i
  done

  if [ -d "$f" ] ; then
    mv "$f"  "$f.0"
    mkdir -m $(stat -c "%a" "$f.0") "$f"
    chown $(stat -c "%u:%g" "$f.0") "$f"
  else
    # We are trying to keep the file rotation time to a minimum
    t=$(mktemp -d -p "$(dirname "$f")")
    mv "$f" "$t/t"
    > "$f"
    # Restore file permissions and ownership
    chmod $(stat -c "%a" "$t/t") "$f"
    chown $(stat -c "%u:%g" "$t/t") "$f"

    # Archive file
    if [ -n "$gz" ] ; then
      gzip < "$t/t" > "$f.0$gz"
      chmod $(stat -c "%a" "$t/t") "$f"
      chown $(stat -c "%u:%g" "$t/t") "$f"
    else
      mv "$t/t" "$f.0"
    fi
    rm -rf "$t"
  fi
}

###$_end-include: rotate.sh

main() {
  local hb_url="" ioredir="" rotlog= rndrun= timer=false

  while [ $# -gt 0 ]
  do
    case "$1" in
      --url=*) hb_url=${1#--url=} ;;
      --ioredir=*) ioredir=${1#--ioredir=} ;;
      --rotate) rotlog=7 ;;
      --rotate=*) rotlog=${1#--rotate=} ;;
      --no-rotate) rotlog= ;;
      --no-rnd-run) rndrun= ;;
      --rnd-run) rndrun=30 ;;
      --rnd-run=*) rndrun=${1#--rnd-run=} ;;
      --time) timer=true ;;
      --no-time) timer=false ;;
      --) shift ; break ;;
      *) break ;;
    esac
    shift
  done

  if [ $# -eq 0 ] ; then
    cat <<-_EOF_
	  Usage:
	      $0 [options] run-command

	  optionns:
	    - --url=url : heartbeat URL
	    - --ioredir=file : redirect stdio to file
	    - --[no-]rotate[=cnt] : rotate stdio log files
	    - --[no-]rnd-run[-secs] : randomize runs
	    - --[no-]time : show time stats at the end
	    - run-command : command to run
	_EOF_
    exit 1
  fi

  [ -n "$rndrun" ] && sleep $(expr $RANDOM % $rndrun)

  if [ -n "$rotlog" ] ; then
    if [ -z "$ioredir" ] ; then
      stderr "rotate settings ignored (no IO redirection required"
    else
      # Rotating
      rotate --count=$rotlog "$ioredir"
    fi
  fi
  [ -n "$ioredir" ] && exec > "$ioredir" 2>&1 </dev/null

  start=$(awk '{ printf "%.0f", $1*1000; }' /proc/uptime)
  if "$@" ; then
    status=up
    msg=OK
  else
    status=down
    msg=FAILED
  fi
  end=$(awk '{ printf "%.0f", $1*1000; }' /proc/uptime)

  if [ -n "$ioredir" ] ; then
    x=$(tail -1 $ioredir)
    [ -n "$x" ] && msg="$x"
  fi
  ping=$(expr $end - $start)
  if [ -n "$hb_url" ] ; then
    local xurl="$(echo "$hb_url" | sed -e s"/{runtime}/$ping/")"
    wget -O- -nv "$xurl"
  fi

  $timer && printf "RUN-TIME: %d.%02d\n" $(expr $ping / 1000) $(expr $ping % 1000 / 10)
}

main "$@"
