#!/bin/sh

# Basic snapshot-style rsync backup script
NSNAPS=14
LATEST=

while [ $# -gt 0 ]
do
  case "$1" in
    --snaps=*) NSNAPS=${1#--snaps=} ;;
    --latest=*) LATEST=${1#--latest=} ;;
    *) break
  esac
  shift
done
if [ $# -ne 2 ] ; then
  echo "Usage:"
  echo "  $0 [--snaps=n] <src> <dst>"
  exit 1
fi
SRC="$1"
DST="$2"

# Config
OPT="-aH --stats -F"
if [ ! -d "$SRC" ] ; then
  echo "$SRC: not found"
  exit 1
fi
if [ ! -d "$DST" ] ; then
  echo "$DST: not found"
  exit 1
fi
SRC=$(readlink -f "$SRC")
DST=$(readlink -f "$DST")
date=`date "+%Y-%m-%d_%T"`

rsync \
  $OPT --link-dest="$DST/last/" \
  "$SRC/" \
  "$DST/$date"

# Remove symlink to previous snapshot
rm -f $DST/last

# Create new symlink to latest snapshot for the next backup to hardlink
ln -s $date $DST/last

# Create latest image...
if [ -n "$LATEST" ] ; then
  cp -al "$DST/last/." "$LATEST.last"
  rm -rf "$LATEST"
  mv "$LATEST.last" "$LATEST"

fi

# Expire entries
ls -1 "$DST" | sort -r | tail +$(expr $NSNAPS + 2) | while read d
do
  rm -rf "$DST/$d"
done
