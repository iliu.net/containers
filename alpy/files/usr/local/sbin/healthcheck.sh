#!/bin/sh
#
# Perform supervisord healthcheck
#
set -euf
( set -o pipefail 2>/dev/null ) && set -o pipefail

check=$(supervisorctl status | grep -v ' RUNNING ' | wc -l) || :
if [ $check -eq 0 ] ; then
  exit 0
else
  exit 1
fi

