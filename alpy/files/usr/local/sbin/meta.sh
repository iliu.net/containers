#!/bin/sh
set -euf -o pipefail
#
# Create meta files containing special file-system metadata
# i.e. file capabilities, permissions, ownership etc.
#
# This is used to be able to backup to media that has limited
# meta data capabilities, i.e. vfat partitions,
# rsync with low priviledge users, idrive backups, etc...
#
# It can also create an inode index that can be used so that we
# can handle file renames in idrive backups.
#
v=
inode=false
verbose=false

while [ $# -gt 0 ]
do
  case "$1" in
    --inode|-I) inode=true ;;
    --no-inode) inode=false ;;
    --verbose|-v) verbose=true ; v=-v ;;
    *) break
  esac
  shift
done

if [ $# -gt 2 -o $# -eq 0 ] ; then
  cat 1>&2 <<-_EOF_
	Usage:
	  $0 [--inode] {dir} [meta]

	Options:
	  --inode|-I: Create inode index
	_EOF_
  exit 1
fi

fstree="$(readlink -f "$1")"
if [ $# -eq 2 ] ; then
  meta="$2"
else
  meta=".meta"
fi
if [ ! -d "$fstree" ] ; then
  echo "$fstree: directory not found" 1>&2
  exit 2
fi

ipcdir=$(mktemp -d)
trap "rm -rf $v $ipcdir" EXIT

runpipe() {
  local fd="$1" ; shift
  mkfifo "$ipcdir/$fd"
  (
    "$@" < "$ipcdir/$fd" &
  )
  eval "exec $fd>"'$ipcdir/$fd'
  rm -f "$ipcdir/$fd"
}

specials() {
  if read -r firstline ; then
    (
      echo "$firstline" | tr '\n' '\0'
      exec tr '\n' '\0'
    ) | ( cd "$1" && cpio -o -H newc -0) | gzip $v > "$2"
  else
    rm -f "$2"
  fi
}

caplist() {
  while read -r firstline
  do
    firstcmd="$(cd "$1" && getcap "$firstline")"
    [ -z "$firstcmd" ] && continue
    # Found stuff...
    (
      echo "$firstcmd"
      tr '\n' '\0'| ( cd "$1" && xargs -0 -r getcap )
    ) | gzip $v > "$2"
    return 0
  done
  rm -f "$2"
}

faclist() {
  if read -r firstline ; then
    (
      echo "$firstline" | tr '\n' '\0'
      exec tr '\n' '\0'
    ) | ( cd "$1" && xargs -0 -r getfacl -p -n ) | gzip $v > "$2"
  else
    rm -f "$2"
  fi
}

gzlist() {
  if read -r firstline ; then
    (
      echo "$firstline"
      exec cat
    ) | gzip $v > "$1"
  else
    rm -f "$1"
  fi
}

ino_fmtnum() {
  local num="$1" fnm='' len=$(expr length "$1")

  while [ $len -gt 0 ]
  do
    pos=$(expr $len - 3)
    if [ $pos -lt 1 ] ; then
      pos=1
    fi
    s=$(expr substr "$num" $pos $len)
    if [ -z "$fnm" ] ; then
      fnm="$s"
    else
      fnm="$s/$fnm"
    fi
    if [ $pos -eq 1 ] ; then
      len=0
      num=''
    else
      len=$(expr $pos - 1)
      num=$(expr substr "$num" 1 $len)
    fi
  done
  echo "$fnm"
}

ino_index() {
  local ino="$1" fpath="$2" ipath="$metadir/inodes/$(ino_fmtnum $ino)"

  if [ ! -f "$ipath.f" ] ; then
    mkdir -p "$(dirname "$ipath")"
    ln "$fstree/$fpath" "$ipath.f"
  fi
  # Create the reverse index
  if [ -f "$ipath.n" ] ; then
    cidx="$(cat "$ipath.n")"
    [ x"$cidx" = x"$fpath" ] && return # Trivial case!
    nidx="$(
      (
	echo "$cidx" ;
	echo "$fpath"
      )|sort -u)"
    if [ x"$cidx" != x"$nidx" ] ; then
      echo "$nidx" > $ipath.n
    fi
  else
    echo "$fpath" > $ipath.n
  fi
}

is_valid() {
  case "$1" in
    directory) return 0 ;;
    regular\ file) return 0 ;;
    regular\ empty\ file) return 0 ;;
  esac
  return 1
}

metadir="$fstree/$meta"
[ ! -d "$metadir" ] && mkdir "$metadir"

runpipe 10 specials "$fstree" "$metadir/specials.cpio.gz"
runpipe 20 caplist "$fstree" "$metadir/caps.txt.gz"
runpipe 30 faclist "$fstree" "$metadir/facl.txt.gz"
runpipe 40 gzlist "$metadir/filelist.txt.gz"

find "$fstree" -print | sed -e "s!^$fstree/!!" -e "s!^$fstree\$!!" | (
  count=0
  while read -r FPATH
  do
    (
      [ -z "$FPATH" ] || [ ! -e "$fstree/$FPATH" ]
    ) && continue
    [ x"$(echo $FPATH | cut -d/ -f1)" = x"$meta" ] && continue

    count=$(expr $count + 1)
    set - $(stat -c '%u:%g %a %h %i %s %F' "$fstree/$FPATH")
    [ $# -lt 6 ] && continue
    usrgrp="$1" ; mode="$2" ; hcnt="$3" ino="$4" sz="$5"; shift 5
    ftype="$*"

    if is_valid "$ftype" ; then
      echo "$usrgrp $mode $hcnt $ino $FPATH" >&40
      echo "$FPATH" >&20
      echo "$FPATH" >&30
      $inode &&  [ x"$ftype" = x"regular file" ] && [ $sz -gt  0 ] && ino_index $ino "$FPATH"
    else
      echo "$FPATH" >&10
    fi
  done
  $verbose && echo "Entries: $count" 1>&2
  :
)
if $inode ; then
  # Scan now the inode index to get rid of removed files
  find "$metadir/inodes" -type f -name "*.[fn]" | sed -e 's/\.[fn]$//' | sort -u | while read -r ipath
  do
    if [ -f "$ipath.n" ] && [ -f "$ipath.f" ] ; then
      # It is there...
      hcnt=$(stat -c '%h' "$ipath.f")
      if [ $hcnt -lt 2 ] ; then
        # File was removed
	rm -f $v "$ipath.n" "$ipath.f"
      else # Not removed...
        # Make sure that "n" is still valid
	cidx="$(cat "$ipath.n")"
	nidx="$(
		exec <"$ipath.n"
		while read -r xpath
		do
		  [ -f "$fstree/$xpath" ] && echo "$xpath" || :
		done
	      )"
	if [ -z "$nidx" ] ; then
	  # This should not happen!
	  rm -f $v "$ipath.n" "$ipath.f"
	elif [ x"$cidx" != x"$nidx" ] ; then
	  # name index changed
	  echo "$nidx" > "$ipath.n"
	fi
      fi
    else
      # Incomplete inode index entry... remove it
      rm -f $v "$ipath.n" "$ipath.f"
    fi
  done
  find "$metadir/inodes" -type d | while read -r dpath
  do
    [  $(ls -1 "$dpath"|wc -l) -eq 0 ] && rmdir $v "$dpath" || :
  done
fi

# Clean-up stuff
exec 10>&- 20>&- 30>&- 40>&-
wait

