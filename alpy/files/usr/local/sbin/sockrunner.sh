#!/bin/sh
#
set -euf -o pipefail
sockpath=""

for i in "$@"
do
  case "$i" in
    unix:*) sockpath=${i#unix:} ; break ;;
  esac
done

if [ -z "$sockpath" ] ; then
  echo "Unknown sock path" 1>&2
  exit 1
fi

if [ -S "$sockpath" ] ; then
  rm -fv "$sockpath"
fi

exec "$@"
