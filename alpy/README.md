# ALPY

Basic Alpine container.  Implements common functionality:

- supervisor w/ healtcheck
- crond
- misc support scripts

## startup.sh

This script is the docker entry point.  It:

- sets the timezone from TZ environment variable
- confirms there is network connectivity
- If a file `/addons.sh` exists, it will source it.
- starts supervisord

