#!/bin/sh
#
# Fixed release containers
#
. ./versions
set -xe

BUILD_X_ARGS="--build-arg alpine_ver=$alpine_ver" \
      sh dockbuild.sh \
	  --tags="$drel-$alpine_ver,$alpine_ver" \
	  --output=docker-images \
	  alpy
sh dockbuild.sh \
	--tags="$drel-$alpine_ver,$alpine_ver" \
	--output=docker-images \
	dovecot
sh dockbuild.sh \
	--tags="$drel-$alpine_ver,$alpine_ver" \
	--output=docker-images \
	ircd
sh dockbuild.sh \
	--output=docker-images \
	netscan
BUILD_X_ARGS="--build-arg phpV=$phpV" \
    sh dockbuild.sh \
	--tags="$drel-$alpine_ver-php$php_ver,$alpine_ver-php$php_ver" \
	--output=docker-images \
	phpsrv
sh dockbuild.sh \
	--tags="$drel-$alpine_ver,$alpine_ver" \
	--output=docker-images \
	rsyncd
sh dockbuild.sh \
	--tags="$drel-$alpine_ver,$alpine_ver" \
	--output=docker-images \
	staticweb
