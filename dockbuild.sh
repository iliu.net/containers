#!/bin/sh
#
# Build it
#
# Example of BUILD_X_ARGS
#
#  --build-arg key=value
#
output=""
tags=""
while [ $# -gt 0 ]
do
  case "$1" in
  --output=*) output=${1#--output=} ;;
  --tags=*) tags=${1#--tags=} ;;
  *) break ;;
  esac
  shift
done

if [ $# -eq 0 ] ; then
  echo "Usage:"
  echo "  $0 [--output=path] directory [name]"
  exit 1
fi
set -euf
( set -o pipefail 2>/dev/null ) && set -o pipefail

DIR="$1"
if [ $# -gt 1 ] ; then
  NAME="$2"
else
  NAME="$(basename "$DIR")"
fi

if [ -d "$output" ] ; then
  output="$output/$NAME"
fi

if [ -f "$DIR/Makefile" ] ; then
  make \
    -C "$DIR" \
    IMAGE="$NAME" \
    BUILD_X_ARGS="${BUILD_X_ARGS:-}" \
    build
else
  docker build -t "$NAME" ${BUILD_X_ARGS:-} "$DIR"
fi
docker save --output="$output" "$NAME"
if [ -n "$tags" ] ; then
  echo '# Additonal tags'
  echo "$tags" | tr ',' "\n" | tee "$output.tags"
fi

