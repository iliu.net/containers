#!/bin/sh
#
# Rolling release containers
#
set -xe
sh dockbuild.sh --output=docker-images voidlinux voidlinux-musl
