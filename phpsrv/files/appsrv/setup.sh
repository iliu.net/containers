#!/bin/sh
#
# Handle start-up routines
#
appsrv=$(dirname "$0")

# Add webapp specific requirements
$appsrv/reqs.sh

# Run any webpass startup scripts...
find /webapps -mindepth 2 -maxdepth 2 \
	-name startup.sh -type f -print | while read f
do
  [ -f "$f" ] && "$f" start
done

# Clean-up old/obsolete links
find /www -mindepth 1 -maxdepth 1 -type l | while read l
do
  [ -e "$l" ] && continue
  rm -fv "$l"
done

# Link /webapps/*/www dir into /www
find /webapps -mindepth 2 -maxdepth 2 -name www -print | while read wwwdir
do
  [ ! -d "$wwwdir" ] && continue
  appname=$(echo "$wwwdir" | cut -d/ -f3)
  if [ -L "/www/$appname" ] ; then
    clink=$(readlink "/www/$appname")
    [ x"$clink" = x"$wwwdir" ] && continue
    rm -f "/www/$appname"
  else
    if [ -e "/www/$appname" ] ; then
      echo "/www/$appname: already exists!" 1>&2
      continue
    fi
  fi
  ln -s "$wwwdir" "/www/$appname"
done
rm -f /www/core ; ln -s ../appsrv/www /www/core


# Tweak permissions of /www/core
find /appsrv/www -type f -print0 | xargs -r -0 chown root:root

# Create backup repositories
mkdir -p /repo/backups /repo/snaps
