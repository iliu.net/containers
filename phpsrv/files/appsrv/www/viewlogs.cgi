#!/usr/bin/haserl
<%
  set -euf
  (set -o pipefail 2>/dev/null) && set -o pipefail

  list_files() {
    echo 'Content-type: text/html'
    echo ''
    echo '<h1>Log files</h1>'
    if [ -n "$*" ] ; then
      echo "<p>$*</p>"
    fi
    echo '<ul>'
    find /var/log -type f | cut -d/ -f4- | sort | while read f
    do
      echo '<li>'
      echo "<a href=\"$SCRIPT_NAME/$f\">$f</a>"
      du -sh /var/log/"$f" | awk '{print $1}'
      echo '</li>'
    done
    echo '</ul>'
    exit;
  }
  dump_log() {
    local log="$(echo "$1" | sed -e 's!^/*!!' -e 's!\.\./!/!')"
    if [ ! -f "/var/log/$log" ] ; then
      list_files "<strong>$log: Not found</strong>"
    fi
    echo 'Content-type: text/plain'
    echo ''
    echo "$log"
    ( for x in $(seq 1 $(expr length "$log")) ; do echo -n = ; done ; echo '')
    cat /var/log/$log
  }

  if [ -z "${PATH_INFO:-}" ] ; then
    # Show an index of available files
    list_files
  else
    dump_log "${PATH_INFO}"
  fi
  # echo Content-type: text/plain
  # echo ''
  # env | sort
%>
