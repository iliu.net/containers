<?php $title = 'PHPINFO'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <!-- link rel="stylesheet" href="style.css" -->
    <!-- <script src="script.js"></script> -->
  </head>
  <body>
    <h1><?= $title ?></h1>
    <a href="/">Home</a>
    <?php phpinfo(); ?>
  </body>
</html>
