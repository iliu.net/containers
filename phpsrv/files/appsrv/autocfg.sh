#!/bin/sh
#
# Auto-config stuff
#
set -euf
( set -o pipefail 2>/dev/null) && set -o pipefail



#
# /webapps/oneofs
# /webapps/*/periodic/*/file => /etc/periodic/*/file
# /webapps/*/nginx.conf
# /webapps/*/apache.conf
# /webapps/*/www
# /webapps/*/setup.sh

#

