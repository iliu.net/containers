#!/bin/sh
#
# Backup webapss code and html
#
backup_target=/repo/backups/data
snaps_target=/repo/snaps/data
apphome=$(readlink -f "$0" | cut -d/ -f-3)
snapback=/usr/local/sbin/snapbackup.sh
cronrun=/usr/local/sbin/cron-run.sh

case "$1" in
cronrun)
  mkdir -p $backup_target
  mkdir -p $snaps_target
  exec $cronrun \
	--ioredir=$backup_target/run.log \
	--rotate=8 \
	--rnd-run=600 \
	--time \
	"$0" backup
  ;;
backup)
  for src in webapps www
  do
    mkdir -p $backup_target/$src
    mkdir -p $snaps_target/$src
    $snapback \
	--latest=$backup_target/$src \
	/$src \
	$snaps_target/$src
  done
  # TODO
  # rsync to mirror?
  ;;
*)
  echo "$1: unknown command" 1>&2
  exit 1
esac


