#!/bin/sh
#
# Handle requirements files
#

#~ # Set-up shared cache first...
#~ version=$(cut -d. -f-2 /etc/alpine-release)
#~ mkdir -p /media/common/alpine-$version/cache
#~ if [ ! -L /etc/apk/cache ] ; then
  #~ ln -s /media/common/alpine-$version/cache /etc/apk/cache
#~ fi
#~ apk update

# We need to determine the php version
phpV=$(cat /etc/apk/world | grep -E '^php[0-9]+$')

# Add any particular requirements
apk add $(
  (
    cat /appsrv/requirements.txt
    find /webapps -mindepth 2 -maxdepth 2 \
	  -name requirements.txt -type f -print0 \
    | xargs -r -0 sed -e "s!\${phpV}!$phpV!g"
  ) | sort -u)

