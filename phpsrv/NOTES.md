


# /www directory

- /webapps/{name}/www (directory or symlink to dir)

# Cron support

- $(webdir)/crontabs
  Contain lines with `user` crontab specification
- $(webdir)/periodic/{15min,daily,hourly,monthly,weekly}/file
  Symlinked to /etc/periodic/...

# Preparation

- requirements.txt
- startup.sh

# nginx configuration

/webapps/*/nginx.conf is included into nginx.conf

***

https://www.ameyalokare.com/docker/2017/09/14/docker-migrating-legacy-links.html

API
- [x] check logs
- run setup
- restart all

WebApps

- [x] CBase
- [x] MinigalNano
- [x] htadmin-mcs
- [x] NacoWiki
  - [x] backup script
  - [ ] rsync backup
- [x] NeuSol
- [x] backup code and www
- [x] Pergamino
- [x] SupervisorUI-mfdev
- [x] supervisorui-redone
- [x] cuy & vms8
- [x] webscan
- [ ] autoidx
- [x] FatFreeFramework on nginx
- [ ] proxy supervisor
- [ ] verify backups

```bash



```
